package com.revolut.rates.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

open class ConnectivityService(context: Context) {

    internal enum class NetworkConnectionType {
        WIFI, MOBILE, NONE
    }

    private var connectivityManager: ConnectivityManager? = null
    private var connectivitySubject = BehaviorSubject.create<Boolean>()

    private var context: Context? = context

    init {
        connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        registerBroadcastReceiver()
    }

    private fun registerBroadcastReceiver() {
        val intentFilter = IntentFilter()
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        intentFilter.addCategory(context?.packageName)
        context?.registerReceiver(NetworkChangeReceiver(), intentFilter,
                "com.google.android.c2dm.permission.SEND", null)
    }

    private fun getNetworkConnectionType(): NetworkConnectionType {

        val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?

        connectivityManager ?: return NetworkConnectionType.NONE

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)

            capabilities ?: return NetworkConnectionType.NONE

            return when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> NetworkConnectionType.WIFI
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> NetworkConnectionType.MOBILE
                else -> NetworkConnectionType.NONE
            }
        } else {
            val currentNetworkInfo = connectivityManager.activeNetworkInfo

            currentNetworkInfo ?: return NetworkConnectionType.NONE

            return when (currentNetworkInfo.getType()) {
                ConnectivityManager.TYPE_WIFI -> NetworkConnectionType.WIFI
                ConnectivityManager.TYPE_MOBILE -> NetworkConnectionType.MOBILE
                else -> NetworkConnectionType.NONE
            }
        }
    }

    private fun isNetworkConnectionAvailable(): Boolean {
        return getNetworkConnectionType() != NetworkConnectionType.NONE
    }

    fun observeAvailability(): Observable<Boolean> {
        return connectivitySubject.startWith(isNetworkConnectionAvailable())
    }

    inner class NetworkChangeReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            connectivitySubject.onNext(isNetworkConnectionAvailable())
        }
    }

}