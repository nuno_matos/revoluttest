package com.revolut.rates.network

import com.revolut.rates.model.RatesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface RatesService {

    @GET("latest")
    fun getRates(@QueryMap baseRate: Map<String, String>? = HashMap()) : Observable<RatesResponse>

}