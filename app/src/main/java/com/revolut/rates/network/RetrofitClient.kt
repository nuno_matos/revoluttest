package com.revolut.rates.network

import com.revolut.rates.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient {

    companion object {

        private val URL = BuildConfig.RATES_URL

        private var retrofitClient: Retrofit? = null

        fun getInstance() : Retrofit {

            if (retrofitClient == null) {
                val clientBuilder = OkHttpClient.Builder()

                if (BuildConfig.DEBUG) {

                    val interceptor = HttpLoggingInterceptor()
                    interceptor.level = HttpLoggingInterceptor.Level.BODY

                    clientBuilder.addInterceptor(interceptor)
                }

                val client = clientBuilder.build()

                retrofitClient = Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client)
                    .build()
            }

            return retrofitClient!!
        }
    }
}