package com.revolut.rates.ui.view

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.revolut.rates.R
import com.revolut.rates.model.RateModel
import kotlinx.android.synthetic.main.view_rate_item.view.*
import java.text.DecimalFormat

class RateView : ConstraintLayout {

    interface OnInputChangedListener {
        fun onInputChanged(input: Double)
    }

    private var onInputChangedListener: OnInputChangedListener? = null
    private var rateModel: RateModel? = null

    private var textWatcher = object : TextWatcher {

        override fun afterTextChanged(edt: Editable?) {
            // Do nothing
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            // Do nothing
        }

        override fun onTextChanged(str: CharSequence?, p1: Int, p2: Int, p3: Int) {

            val input = str?.toString() ?: ""

            try {
                val inputDouble = if (input.isNotBlank()) input.toDouble() else 0.00
                onInputChangedListener?.onInputChanged(inputDouble)
            } catch(e: NumberFormatException) {
                onInputChangedListener?.onInputChanged(0.00)
            }
        }
    }

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        View.inflate(context, R.layout.view_rate_item, this)
    }

    fun setContent(rateModel: RateModel) {
        this.rateModel = rateModel
        rateName.text = rateModel.name
    }

    fun updateInput(input: Double) {
        rateValue?.text = input.toString()
        inputRateValue?.setText(input.toString())
        invalidate()
    }

    fun updateRateValue(value: Double) {
        if (rateModel != null) {
            val calculatedValue = value * (rateModel!!.value ?: 0.00)
            val format = DecimalFormat("0.00")
            rateValue?.text = format.format(calculatedValue).replace(",", ".")
            inputRateValue?.setText(format.format(calculatedValue).replace(",", "."))
            invalidate()
        }
    }

    fun setFocusOnItem(onInputChangedListener: OnInputChangedListener) {
        rateValue?.visibility = View.GONE
        inputRateValue?.visibility = View.VISIBLE
        inputRateValue?.addTextChangedListener(textWatcher)
        this.onInputChangedListener = onInputChangedListener
    }

    fun removeFocusOnItem() {
        rateValue?.visibility = View.VISIBLE
        inputRateValue?.visibility = View.INVISIBLE
        inputRateValue?.removeTextChangedListener(textWatcher)
        onInputChangedListener = null
    }
}