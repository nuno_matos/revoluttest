package com.revolut.rates.ui.main

import com.revolut.rates.model.RateModel
import com.revolut.rates.ui.base.BaseContract

interface MainContract {

    interface View : BaseContract.View {
        fun onRateListReceived(rateList: List<RateModel>, updatedBase: Boolean)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun loadRates()
        fun changeBaseRate(baseRate: String)
    }

}