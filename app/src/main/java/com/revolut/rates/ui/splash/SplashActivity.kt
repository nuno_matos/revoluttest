package com.revolut.rates.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.revolut.rates.R
import com.revolut.rates.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    companion object {
        private const val FADE_ANIMATION_DURATION = 400L
        private const val SHOW_IMAGE_DURATION = 800L
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        configureUI()
        startFadeInAnimation()
    }

    private fun configureUI() {
        container.setOnClickListener { skipSplash() }
    }

    private fun startFadeInAnimation() {
        splashImage ?: return

        splashImage.animate()
                .alpha(1f)
                .setStartDelay(FADE_ANIMATION_DURATION)
                .setDuration(FADE_ANIMATION_DURATION)
                .withEndAction { startFadeOutAnimation() }
                .start()
    }

    private fun startFadeOutAnimation() {
        splashImage ?: return

        splashImage.animate()
                .alpha(0f)
                .setStartDelay(SHOW_IMAGE_DURATION)
                .setDuration(FADE_ANIMATION_DURATION)
                .withEndAction { skipSplash() }
                .start()
    }

    private fun skipSplash() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}