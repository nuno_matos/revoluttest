package com.revolut.rates.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.revolut.rates.R
import com.revolut.rates.model.RateModel
import com.revolut.rates.ui.view.RateView

class RateListAdapter(private var rateItems: List<RateModel>,
                      private var onRateClicked: (RateModel) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), RateView.OnInputChangedListener {

    private var value: Double? = 0.00

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return RateViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rate_item, parent, false))
    }

    override fun getItemCount() = rateItems.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? RateViewHolder)?.bind(rateItems[position])
    }

    override fun onInputChanged(input: Double) {
        value = input

        // update every item's value except the first one because the user is changing its value manually
        notifyItemRangeChanged(1, itemCount - 1)
    }

    fun setItems(rateItems: List<RateModel>) {
        this.rateItems = rateItems
    }

    inner class RateViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val rateItemContainer: FrameLayout = itemView.findViewById(R.id.rateItemContainer)

        fun bind(rateModel: RateModel) = with(itemView) {

            rateItemContainer.removeAllViews()
            val rateItem = RateView(context)

            rateItem.setContent(rateModel)

            if (adapterPosition == 0) {
                rateItem.updateInput(value!!)
                rateItem.setFocusOnItem(this@RateListAdapter)
            } else {
                rateItem.removeFocusOnItem()
                rateItem.updateRateValue(value!!)
            }

            setOnClickListener {
                onRateClicked(rateModel)
            }

            rateItemContainer.addView(rateItem)
        }
    }
}