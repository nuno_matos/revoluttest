package com.revolut.rates.ui.main

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.revolut.rates.R
import com.revolut.rates.model.RateModel
import com.revolut.rates.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainContract.View, MainContract.Presenter>(), MainContract.View {

    companion object {
        private const val FLIPPER_CONTENT_INDEX = 0
        private const val FLIPPER_LOADING_INDEX = 1
        private const val FLIPPER_EMPTY_VIEW_INDEX = 2
        private const val FLIPPER_GENERIC_ERROR_INDEX = 3
    }

    private var adapter: RateListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        configureUI()
        loadContent()
    }

    override fun createPresenter(): MainContract.Presenter {
        return MainPresenter()
    }

    override fun onRateListReceived(rateList: List<RateModel>, updatedBase: Boolean) {

        if (adapter == null) {

            adapter = RateListAdapter(rateList) { rate ->
                presenter?.changeBaseRate(rate.name ?: "")
            }

            rateItemsList.adapter = adapter

        } else {
            adapter!!.setItems(rateList)

            if (updatedBase) {
                adapter!!.notifyDataSetChanged()
            } else {
                adapter!!.notifyItemRangeChanged(1, adapter!!.itemCount - 1)
            }
        }
    }

    override fun showContent() {
        if (flipper.displayedChild != FLIPPER_CONTENT_INDEX) {
            flipper.displayedChild = FLIPPER_CONTENT_INDEX
        }
    }

    override fun showLoading() {
        flipper.displayedChild = FLIPPER_LOADING_INDEX
    }

    override fun showEmptyView() {
        flipper.displayedChild = FLIPPER_EMPTY_VIEW_INDEX
    }

    override fun onNetworkConnectionChanged(hasInternet: Boolean) {

        // show/hide the internet icon
        networkIcon.animate()
                .alpha(if (hasInternet) 0f else 1f)
                .setDuration(500L)
                .start()
    }

    override fun showGenericError() {
        flipper.displayedChild = FLIPPER_GENERIC_ERROR_INDEX
    }

    private fun configureUI() {

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = RecyclerView.VERTICAL
        rateItemsList.layoutManager = layoutManager

        val emptyView = flipper.getChildAt(FLIPPER_EMPTY_VIEW_INDEX)
        val genericErrorView = flipper.getChildAt(FLIPPER_GENERIC_ERROR_INDEX)

        emptyView.setOnClickListener { loadContent() }
        genericErrorView.setOnClickListener { loadContent() }
    }

    private fun loadContent() {
        presenter?.loadRates()
    }
}
