package com.revolut.rates.ui.main

import android.util.Log
import com.revolut.rates.app.RatesApplication
import com.revolut.rates.interactor.RatesInteractor
import com.revolut.rates.network.ConnectivityService
import com.revolut.rates.ui.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainPresenter : BasePresenter<MainContract.View>(), MainContract.Presenter {

    companion object {
        private val TAG = MainPresenter::class.java.canonicalName

        private const val UPDATE_TIME_INTERVAL = 1000L
    }

    @Inject
    lateinit var connectivityService: ConnectivityService
    @Inject
    lateinit var ratesInteractor: RatesInteractor

    private var disposable: Disposable? = null

    private var baseRateSubject = BehaviorSubject.create<String>()

    private var baseUpdated = false

    init {
        RatesApplication.component?.inject(this)

        connectivityService.observeAvailability()
                .distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            view?.onNetworkConnectionChanged(it)
                        },
                        onError = {
                            Log.d(TAG, it.message ?: "Error updating network status")
                        }
                )
    }

    override fun loadRates() {
        view?.showLoading()

        // get the first set of rates with no base rate
        ratesInteractor.getRates("")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy (
                        onNext = {rateList ->
                            view?.showContent()
                            view?.onRateListReceived(rateList, true)
                            val base = rateList.first()
                            startLoadingRates(base.name ?: "")
                        },
                        onError = {
                            view?.showGenericError()
                        })
    }

    override fun changeBaseRate(baseRate: String) {

        //when the base rate is changed, we set this flag to true so the whole list can be updated
        baseUpdated = true

        baseRateSubject.onNext(baseRate)
    }

    private fun startLoadingRates(base: String) {

        baseRateSubject
                .startWith(base)
                .distinctUntilChanged()
                .subscribeBy (
                        onNext = { baseRate ->

                            // cancel possible pending request
                            disposable?.dispose()

                            disposable = ratesInteractor.getRates(baseRate)
                                    .distinctUntilChanged()
                                    .delay(UPDATE_TIME_INTERVAL, TimeUnit.MILLISECONDS)
                                    .repeat()
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribeBy (
                                            onNext = {rateList ->

                                                if (rateList.isNotEmpty()) {
                                                    view?.showContent()
                                                    view?.onRateListReceived(rateList, baseUpdated)

                                                    if (baseUpdated) {
                                                        baseUpdated = false
                                                    }
                                                } else {
                                                    view?.showEmptyView()
                                                }
                                            },
                                            onError = {
                                                // just log the error because we already have information to display
                                                Log.d(TAG, it.message ?: "Error while updating rates")
                                            })
                        },
                        onError = {
                            // just log the error because we already have information to display
                            Log.d(TAG, it.message ?: "Error while updating rates")
                        }
        )
    }
}