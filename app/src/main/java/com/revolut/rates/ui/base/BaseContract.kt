package com.revolut.rates.ui.base

interface BaseContract {

    interface View {
        fun onNetworkConnectionChanged(hasInternet: Boolean)
        fun showContent()
        fun showLoading()
        fun showEmptyView()
        fun showGenericError()
    }

    interface Presenter<V : View> {
        fun attachView(view: V)
        fun detachView()
    }
}