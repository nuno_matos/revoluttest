package com.revolut.rates.app

import android.app.Application

class RatesApplication : Application() {

    companion object {
        var component: RatesComponent? = null
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerRatesComponent.builder()
            .ratesModule(RatesModule(applicationContext))
            .build()
    }

}