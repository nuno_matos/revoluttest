package com.revolut.rates.app

import android.content.Context
import com.revolut.rates.interactor.RatesInteractor
import com.revolut.rates.network.ConnectivityService
import dagger.Module
import dagger.Provides

@Module
class RatesModule(context: Context) {

    private var context: Context? = context

    @Provides
    fun provideContext(): Context {
        return context!!
    }

    @Provides
    fun provideConnectivityService(context: Context): ConnectivityService {
        return ConnectivityService(context)
    }

    @Provides
    fun provideRatesInteractor(): RatesInteractor {
        return RatesInteractor()
    }
}