package com.revolut.rates.app

import com.revolut.rates.ui.main.MainPresenter
import dagger.Component

@Component(modules=[RatesModule::class])
interface RatesComponent {
    fun inject(presenter: MainPresenter)
}