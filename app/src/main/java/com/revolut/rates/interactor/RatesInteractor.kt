package com.revolut.rates.interactor

import android.text.TextUtils
import com.revolut.rates.model.RateModel
import com.revolut.rates.network.RatesService
import com.revolut.rates.network.RetrofitClient
import io.reactivex.Observable

class RatesInteractor {

    private var ratesService: RatesService? = null

    init {
        ratesService = RetrofitClient.getInstance().create(RatesService::class.java)
    }

    fun getRates(baseRate: String?) : Observable<List<RateModel>> {

        ratesService ?: return Observable.just(ArrayList())

        val queryMap = HashMap<String, String>()

        // if we have a selected base rate, add it to the request
        if (!TextUtils.isEmpty(baseRate)) {
            queryMap["base"] = baseRate!!
        }

        return ratesService!!.getRates(queryMap).map { ratesResponse ->

            val rates = ratesResponse.rates
            val ratesList = ArrayList<RateModel>()

            rates ?: return@map ratesList

            ratesList.add(RateModel(ratesResponse.base ?: "", null))

            // each key corresponds to the rate name
            for (key in rates.keySet()) {
                val rateValue = rates[key].toString().toDouble()
                ratesList.add(RateModel(key, rateValue))
            }

            return@map ratesList
        }
    }
}