package com.revolut.rates.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.Gson
import com.google.gson.JsonObject


data class RatesResponse(
    val base: String? = null,
    val date: String? = null,
    val rates: JsonObject? = null) : Parcelable {

    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        Gson().fromJson(source.readString(), JsonObject::class.java)
    )

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(base)
        writeString(date)
        writeString(rates.toString())
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RatesResponse> {
        override fun createFromParcel(parcel: Parcel): RatesResponse {
            return RatesResponse(parcel)
        }

        override fun newArray(size: Int): Array<RatesResponse?> {
            return arrayOfNulls(size)
        }
    }
}